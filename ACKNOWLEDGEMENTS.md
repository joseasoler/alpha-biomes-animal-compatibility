# Acknowledgements

Developing a RimWorld mod is a huge task that is only made possible because its awesome community shares their expertise and knowledge with others. There are also people and groups outside of the RimWorld community that have been helpful in creating this mod.  Hopefully, all of them should be listed below. Please create a new issue report if anyone is missing!

* **[Erin](https://steamcommunity.com/id/erinmh/myworkshopfiles/?appid=294100)** - [Birds Beyond: Temperate Forest](https://steamcommunitzy.com/sharedfiles/filedetails/?id=2591791695), [Birds Beyond: Tropical Forest](https://steamcommunity.com/sharedfiles/filedetails/?id=2794752505), [Erin's Amaro](https://steamcommunity.com/sharedfiles/filedetails/?id=2791810116), [Erin's Friendly Ferrets](https://steamcommunity.com/sharedfiles/filedetails/?id=2433755150), [Opossum Friends](https://steamcommunity.com/sharedfiles/filedetails/?id=2718423420), [ReGrowth Remastered: Extinct Animals](https://steamcommunity.com/sharedfiles/filedetails/?id=2762496067)
* **[Fluffy](https://steamcommunity.com/id/FluffyMods/myworkshopfiles/?appid=294100)** - [How to report bugs in mods guide](https://steamcommunity.com/sharedfiles/filedetails/?id=725234314)
* **[Helixien](https://steamcommunity.com/id/Helixien/myworkshopfiles/?appid=294100)** - [ReGrowth Remastered: Extinct Animals](https://steamcommunity.com/sharedfiles/filedetails/?id=2762496067)
* **[Luizi](https://steamcommunity.com/profiles/76561198117475921/myworkshopfiles?appid=294100)** - [ReGrowth Remastered: Extinct Animals](https://steamcommunity.com/sharedfiles/filedetails/?id=2762496067)
* **[Nadia Eghbal](https://github.com/nayafia)** - [Contributing Guides: A Template](https://github.com/nayafia/contributing-template)
* **[Nicolas Brown](https://github.com/njbrown)** - [TextureLab](https://www.texturelab.io/): used for the preview image.
* **[Oracle of Thessia](https://steamcommunity.com/id/oracleofthessia/myworkshopfiles/?appid=294100)** - [Opossum Friends](https://steamcommunity.com/sharedfiles/filedetails/?id=2718423420)
* **[RimWorld Discord](https://discord.gg/rimworld)**
* **[RimWorld Subreddit](https://www.reddit.com/r/RimWorld/)**
* **[Sarg Bjornson](https://steamcommunity.com/profiles/76561197984862442/myworkshopfiles/?appid=294100)** - [Alpha Biomes](https://steamcommunity.com/sharedfiles/filedetails/?id=1841354677)
* **[Semantic versioning](https://semver.org/)**
* **[UnlimitedHugs](https://github.com/UnlimitedHugs/)** - [HugsLib](https://github.com/UnlimitedHugs/RimworldHugsLib)
* **[Vanilla Expanded Series community](https://www.patreon.com/oskarpotocki)**
* **[Vicky Brasseur](https://www.vmbrasseur.com)** - [Forge your future with Open Source](https://pragprog.com/titles/vbopens/forge-your-future-with-open-source/)
* **[xrushha](https://steamcommunity.com/id/xrushha/myworkshopfiles/?appid=294100)** - [ReGrowth Remastered: Extinct Animals](https://steamcommunity.com/sharedfiles/filedetails/?id=2762496067)
* **[Zeitloser](https://steamcommunity.com/profiles/76561198119242914/myworkshopfiles/?appid=294100)** - [Birds Beyond: Temperate Forest](https://steamcommunitzy.com/sharedfiles/filedetails/?id=2591791695), [Birds Beyond: Tropical Forest](https://steamcommunity.com/sharedfiles/filedetails/?id=2794752505)
