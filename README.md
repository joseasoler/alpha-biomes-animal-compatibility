Alpha Biomes Animal Compatibility
===

[![RimWorld](https://img.shields.io/badge/RimWorld-1.3-informational)](https://rimworldgame.com/) [![Steam Downloads](https://img.shields.io/steam/downloads/2853481857)](https://steamcommunity.com/sharedfiles/filedetails/?id=2853481857) [![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT) [![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-2.1-4baaaa.svg)](CODE_OF_CONDUCT.md)

**Update:** The content of this mod has been submitted and accepted into Alpha Biomes. This mod is now unlisted, and it adds no content. Please unsubscribe from it when possible!

### Previous description


This [RimWorld](https://rimworldgame.com/) mod patches animals coming from the following mods to make them appear naturally in the biomes included in [Alpha Biomes](https://steamcommunity.com/sharedfiles/filedetails/?id=1841354677):

* [Birds Beyond: Temperate Forest](https://steamcommunitzy.com/sharedfiles/filedetails/?id=2591791695)
* [Birds Beyond: Tropical Forest](https://steamcommunity.com/sharedfiles/filedetails/?id=2794752505)
* [Erin's Amaro](https://steamcommunity.com/sharedfiles/filedetails/?id=2791810116)
* [Erin's Carbuncles](https://steamcommunity.com/sharedfiles/filedetails/?id=2857745758)
* [Erin's Fox Squirrel](https://steamcommunity.com/sharedfiles/filedetails/?id=2092951979)
* [Erin's Friendly Ferrets](https://steamcommunity.com/sharedfiles/filedetails/?id=2433755150)
* [Opossum Friends](https://steamcommunity.com/sharedfiles/filedetails/?id=2718423420)
* [ReGrowth Remastered: Extinct Animals](https://steamcommunity.com/sharedfiles/filedetails/?id=2762496067)



Contributions
---

This project encourages community involvement and contributions. Check the [CONTRIBUTING](CONTRIBUTING.md) file for details. Existing contributors can be checked in the [contributors list](https://gitlab.com/joseasoler/alpha-biomes-animal-compatibility/-/graphs/main).

License
---

This project is licensed under the MIT license. Check the [LICENSE](LICENSE) file for details.

Acknowledgements
---

Read the [ACKNOWLEDGEMENTS](ACKNOWLEDGEMENTS.md) file for details.
